FROM jenkins/jenkins:lts-alpine

USER root

ENV GLIBC 2.28-r0
ENV DOCKER_CHANNEL stable
ENV DOCKER_VERSION 19.03.6
ENV DOCKER_COMPOSE_VER 1.27.4
ENV TERRAFORM_VER 0.11.7

# TODO ENV DOCKER_SHA256
# https://github.com/docker/docker-ce/blob/5b073ee2cf564edee5adca05eee574142f7627bb/components/packaging/static/hash_files !!
# (no SHA file artifacts on download.docker.com yet as of 2017-06-07 though)

RUN set -ex; \
	# why we use "curl" instead of "wget":
	# + wget -O docker.tgz https://download.docker.com/linux/static/stable/x86_64/docker-17.03.1-ce.tgz
	# Connecting to download.docker.com (54.230.87.253:443)
	# wget: error getting response: Connection reset by peer
	apk add --no-cache --virtual .fetch-deps \
	curl libcurl \
	wget \
	sudo \
	tar \
	jq \
	groff less python2 py-pip \
	unzip \
	openssl ca-certificates \
	; \
	wget -O /usr/local/bin/docker-compose https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VER}/docker-compose-`uname -s`-`uname -m`; \
	chmod +x /usr/local/bin/docker-compose; \
	# this "case" statement is generated via "update.sh"
	apkArch="$(apk --print-arch)"; \
	case "$apkArch" in \
	x86_64) dockerArch='x86_64' ;; \
	s390x) dockerArch='s390x' ;; \
	*) echo >&2 "error: unsupported architecture ($apkArch)"; exit 1 ;;\
	esac; \
	\
	wget -O docker.tgz https://download.docker.com/linux/static/${DOCKER_CHANNEL}/${dockerArch}/docker-${DOCKER_VERSION}.tgz; \
	\
	tar --extract \
	--file docker.tgz \
	--strip-components 1 \
	--directory /usr/local/bin/ \
	; \
	rm docker.tgz; \
	dockerd -v; \
	docker -v; \
	wget https://releases.hashicorp.com/terraform/${TERRAFORM_VER}/terraform_${TERRAFORM_VER}_linux_amd64.zip; \
	unzip terraform_${TERRAFORM_VER}_linux_amd64.zip -d /usr/local/bin/; \
	pip install awscli requests j2cli boto3

USER ${user}
